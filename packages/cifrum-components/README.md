# `@4kda/vuetify-cifrum-components`

## Usage

```js
import Vue from 'vue'
import CfrComponents from "@4kda/vuetify-cifrum-components";

Vue.use(CfrComponents)
```

### Nuxt
Для исправления "$attrs is readonly" and "$listeners is readonly" необходимо дополнительно добавить в nuxt.config.js:
```js
const path = require('path')

export default {
    //...
    build: {
        extend(config, {isClient}) {
            // Extend only webpack config for client-bundle
            if (isClient) {
                config.devtool = 'source-map'
            }
            config.resolve.alias['vue'] = path.resolve('./node_modules/vue')
        }
    }
}
```
