import CfrSelect from './components/form/Select'
import CfrNavigationDrawer from './components/layout/nav-drawer/NavDrawer'
import CfrHeader from './components/layout/header/Header'
import CfrMenuActions from './components/MenuActions'
import CfrContentScrollShadowTop from './components/ContentScrollShadowTop'

const components = {
  CfrHeader,
  CfrNavigationDrawer,
  CfrSelect,
  CfrMenuActions,
  CfrContentScrollShadowTop
}

// Declare install function executed by Vue.use()
export function install(Vue) {
  if (install.installed) return;
  install.installed = true;

  Object.entries(components).forEach(([componentName, component]) => {
    Vue.component(componentName, component)
  })
}

// Create module definition for Vue.use()
const plugin = {
  install,
  ...components
};

// Auto-install when vue is found (eg. in browser via <script> tag)
let GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
}

// Default export is library as a whole, registered via Vue.use()
export default plugin;

// Allow component use individually
export const CfrComponents = components;
