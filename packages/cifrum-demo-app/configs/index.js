import theme from './theme'
import locales from './locales'
import navigation from './navigation'

export default {
  // product display information
  product: {
    name: 'Дизайн система ЧУ Цифрум РосАтом',
    version: '0.0.1'
  },

  // theme configs
  theme,

  // locales configs
  locales,

  // navigation configs
  navigation
}
