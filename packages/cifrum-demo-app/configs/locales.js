import en from '../translations/en'
import ru from '../translations/ru'

export default {
  // current locale
  locale: 'ru',

  // when translation is not available fallback to that locale
  fallbackLocale: 'en',

  // availabled locales for user selection
  availableLocales: [{
    code: 'en',
    flag: 'us',
    name: 'English'
  }, {
    code: 'ru',
    flag: 'ru',
    name: 'Русский'
  }],

  messages: {
    en,
    ru
  }
}
