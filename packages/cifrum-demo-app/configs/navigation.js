import menuUI from './menus/ui.menu'

export default {
  // main navigation - side menu
  menu: [
    {
      text: 'Администрирование',
      key: '',
      items: [
        {
          icon: '$ym',
          text: 'Главная',
          link: '/',
        },
        {
          icon: '$knowledgeOutline',
          text: 'База знаний',
          items: [
            {
              icon: 'mdi-cube-outline',
              text: 'Эксперты',
              link: '/experts',
            },
          ],
        },
        {
          icon: '$cloudOutline',
          text: 'Мое пространство',
          link: '/space',
        },
      ],
    },
    {
      text: 'Development',
      key: '',
      perms: ['role:developer'],
      items: [
        {
          icon: 'mdi-cube-outline',
          text: 'UI - Theme Preview',
          items: menuUI,
        },
        // {
        //   icon: 'mdi-file-outline',
        //   text: 'Pages',
        //   key: 'menu.pages',
        //   items: menuPages
        // }
      ],
    },
  ],

  // footer links
  // footer: [{
  //   text: 'Docs',
  //   key: 'menu.docs',
  //   href: 'https://vuetifyjs.com',
  //   target: '_blank',
  //   perms: ['role:developer']
  // }]
}
