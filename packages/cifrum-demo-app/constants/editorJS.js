const EditorJSHeader = process.client ? require('@editorjs/header') : null
const EditorJSList = process.client ? require('@editorjs/list') : null
const EditorJSTable = process.client ? require('@editorjs/table') : null
const EditorJSLink = process.client ? require('@editorjs/link') : null
const EditorJSDelimiter = process.client ? require('@editorjs/delimiter') : null
const EditorJSEmbed = process.client ? require('@editorjs/embed') : null
const EditorJSRaw = process.client ? require('@editorjs/raw') : null
const EditorJSQuote = process.client ? require('@editorjs/quote') : null
const EditorJSImage = process.client ? require('@editorjs/image') : null

export const EditorJS = process.client ? require('@editorjs/editorjs') : null
export const getToolsOptions = apiBaseUrl => ({
  header: {
    class: EditorJSHeader,
    inlineToolbar: true
  },
  list: {
    class: EditorJSList,
    inlineToolbar: true
  },
  linkTool: {
    class: EditorJSLink
  },
  table: {
    class: EditorJSTable
  },
  delimiter: {
    class: EditorJSDelimiter
  },
  embed: {
    class: EditorJSEmbed
  },
  raw: {
    class: EditorJSRaw
  },
  quote: {
    class: EditorJSQuote,
    config: {
      quotePlaceholder: 'Введите цитату',
      captionPlaceholder: 'Автор цитаты'
    }
  },
  image: {
    class: EditorJSImage,
    config: {
      endpoints: {
        byFile: `${process.env.API_HOST}${process.env.API_BASE}/upload/topics/image`,
        byUrl: `${process.env.API_HOST}${process.env.API_BASE}/upload/topics/image-by-url`
      },
      buttonContent: 'Выберите фото'
    }
  }
})

export const getCompactToolsOptions = () => ({
  header: {
    class: EditorJSHeader,
    // inlineToolbar: ['link']
    inlineToolbar: true
  },
  list: {
    class: EditorJSList,
    inlineToolbar: true
  }
})
