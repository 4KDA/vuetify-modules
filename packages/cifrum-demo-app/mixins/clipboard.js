export const clipboardMixin = {
  methods: {
    onCopySuccess (e) {
      this.$dialog.notify.success(`Скопировано: ${e.text}`, {
        position: 'bottom-right',
        timeout: 5000
      })
    },
    onCopyError () {
      this.$dialog.notify.error('При копировании произошла ошибка', {
        position: 'bottom-right',
        timeout: 5000
      })
    },
    copyToClipboard (text) {
      this.$copyText(text).then((e) => {
        this.onCopySuccess(e)
      }, () => {
        this.onCopyError()
      })
    },
  }
}
