export const commonMixin = {
  methods: {
    /**
     * Вызывает метод текущего instance по названию
     * @param name - название метода
     * @param params - массив передаваемых в метод параметров
     */
    methodCallByName (name, ...params) {
      if (this.$options.methods[name]) { // guard to prevent runtime errors
        this.$options.methods[name].call(this, ...params) // Вызываем с сохранением текущего контекста
      }
    }
  }
}
