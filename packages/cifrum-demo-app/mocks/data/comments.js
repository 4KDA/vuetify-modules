const casual = require("casual-browserify").ru_RU;
const faker = require("faker/locale/ru");

const comments = [];


const countTopLevel = casual.integer(1, 15)
const count = casual.integer(1, 100)

// Верхнеуровневые, у которых parent = null
for (let i = 0; i < countTopLevel; i++) {
  const position = faker.name.jobType()
  const comment = {
    id: i++,
    content: {
      blocks: [
        {
          "id": "TKmkbMvYyr",
          "type": "paragraph",
          "data": {
            "text": faker.lorem.paragraph()
          }
        },
        {
          "id": "XfVCbAz7iu",
          "type": "paragraph",
          "data": {
            "text": faker.lorem.paragraph()
          }
        }
      ]
    },
    created_at: faker.datatype.datetime(),
    created_by: {
      id: casual.integer(1, 1000),
      name: casual.full_name,
      avatar: faker.image.avatar(),
      position: position[0].toUpperCase() + position.slice(1),
    },
    parent: null,
    reply_count: casual.integer(1, 1000),
    likes: casual.integer(1, 1000),
    like: faker.datatype.boolean()
  };

  comments.push(comment);
}

for (let i = countTopLevel + 1; i < count; i++) {
  const position = faker.name.jobType()
  const comment = {
    id: countTopLevel + i++,
    content: {
      blocks: [
        {
          "id": "TKmkbMvYyr",
          "type": "paragraph",
          "data": {
            "text": faker.lorem.paragraph()
          }
        },
        {
          "id": "XfVCbAz7iu",
          "type": "paragraph",
          "data": {
            "text": faker.lorem.paragraph()
          }
        }
      ]
    },
    created_at: faker.datatype.datetime(),
    created_by: {
      id: casual.integer(1, 1000),
      name: casual.full_name,
      avatar: faker.image.avatar(),
      position: position[0].toUpperCase() + position.slice(1),
    },
    parent: casual.integer(1, countTopLevel),
    reply_count: casual.integer(1, 1000),
    likes: casual.integer(1, 1000),
    like: faker.datatype.boolean()
  };

  comments.push(comment);
}

export default comments;

export const comment = comments[0]
