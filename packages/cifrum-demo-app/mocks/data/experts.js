const casual = require('casual-browserify').ru_RU;
const faker = require('faker/locale/ru');

const experts = [];

for (let i = 0; i < 100; i++) {
  const position = faker.name.jobType()
  const user = {
    id: i++,
    email: casual.email,
    name: casual.full_name,
    favorite: faker.datatype.boolean(),
    avatar: faker.image.avatar(),
    position: position[0].toUpperCase() + position.slice(1),
    url: faker.internet.url
  };
  experts.push(user)
}

export default experts

// const def =[
//   {
//     id: 1,
//     name: "Абрамов Виктор Сергеевич",
//     position: "Эксперт",
//     favorite: true,
//     avatar: "https://avatars0.githubusercontent.com/u/9064066?v=4&s=460",
//     url: "https://avatars0.githubusercontent.com/u/9064066?v=4&s=460"
//   },
//   {
//     id: 2,
//     name: "Абрамов Виктор Сергеевич",
//     position: "Эксперт",
//     favorite: false,
//     avatar: "https://cdn.vuetifyjs.com/images/john.jpg",
//     url: "https://cdn.vuetifyjs.com/images/john.jpg"
//   },
//   {
//     id: 3,
//     name: "Абрамов Виктор Сергеевич",
//     position: "Эксперт",
//     favorite: false,
//     avatar: "https://yablyk.com/wp-content/uploads/2015/05/tim-cook1.jpg",
//     url: "https://yablyk.com/wp-content/uploads/2015/05/tim-cook1.jpg"
//   },
//   {
//     id: 4,
//     name: "Абрамов Виктор Сергеевич",
//     position: "Эксперт",
//     favorite: true,
//     avatar: "https://avatars0.githubusercontent.com/u/9064066?v=4&s=460",
//     url: "https://avatars0.githubusercontent.com/u/9064066?v=4&s=460"
//   },
//   {
//     id: 5,
//     name: "Абрамов Виктор Сергеевич",
//     position: "Эксперт",
//     favorite: false,
//     avatar: "https://cdn.vuetifyjs.com/images/john.jpg",
//     url: "https://cdn.vuetifyjs.com/images/john.jpg"
//   },
//   {
//     id: 6,
//     name: "Абрамов Виктор Сергеевич",
//     position: "Эксперт",
//     favorite: false,
//     avatar: "https://yablyk.com/wp-content/uploads/2015/05/tim-cook1.jpg",
//     url: "https://yablyk.com/wp-content/uploads/2015/05/tim-cook1.jpg"
//   },
//   {
//     id: 7,
//     name: "Борисов Виктор Сергеевич",
//     position: "Эксперт",
//     favorite: true,
//     avatar: "https://avatars0.githubusercontent.com/u/9064066?v=4&s=460",
//     url: "https://avatars0.githubusercontent.com/u/9064066?v=4&s=460"
//   },
//   {
//     id: 8,
//     name: "Борисов Виктор Сергеевич",
//     position: "Эксперт",
//     favorite: false,
//     avatar: "https://cdn.vuetifyjs.com/images/john.jpg",
//     url: "https://cdn.vuetifyjs.com/images/john.jpg"
//   },
//   {
//     id: 9,
//     name: "Борисов Виктор Сергеевич",
//     position: "Эксперт",
//     favorite: false,
//     avatar: "https://yablyk.com/wp-content/uploads/2015/05/tim-cook1.jpg",
//     url: "https://yablyk.com/wp-content/uploads/2015/05/tim-cook1.jpg"
//   },
//   {
//     id: 10,
//     name: "Борисов Виктор Сергеевич",
//     position: "Эксперт",
//     favorite: true,
//     avatar: "https://avatars0.githubusercontent.com/u/9064066?v=4&s=460",
//     url: "https://avatars0.githubusercontent.com/u/9064066?v=4&s=460"
//   },
//   {
//     id: 11,
//     name: "Борисов Виктор Сергеевич",
//     position: "Эксперт",
//     favorite: false,
//     avatar: "https://cdn.vuetifyjs.com/images/john.jpg",
//     url: "https://cdn.vuetifyjs.com/images/john.jpg"
//   },
//   {
//     id: 12,
//     name: "Борисов Виктор Сергеевич",
//     position: "Эксперт",
//     favorite: false,
//     avatar: "https://yablyk.com/wp-content/uploads/2015/05/tim-cook1.jpg",
//     url: "https://yablyk.com/wp-content/uploads/2015/05/tim-cook1.jpg"
//   }
// ];
