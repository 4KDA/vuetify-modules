const casual = require("casual-browserify").ru_RU;
const faker = require("faker/locale/ru");

const posts = [];
const count = casual.integer(1, 100)

for (let i = 0; i < count; i++) {
  const post = {
    id: i++,
    title: faker.lorem.sentence(),
    img: faker.image.imageUrl(),
    tags: casual.array_of_words(7),
    content: {
      blocks: [
        {
          "id": "7iXmU9jURL",
          "type": "header",
          "data": {
            "text": "Editor.js",
            "level": 2
          }
        },
        {
          "id": "TKmkbMvYyr",
          "type": "paragraph",
          "data": {
            "text": faker.lorem.paragraph()
          }
        },
        {
          "id": "XfVCbAz7iu",
          "type": "header",
          "data": {
            "text": "Key features",
            "level": 3
          }
        },
        {
          "id": "7_blLy8tnY",
          "type": "list",
          "data": {
            "style": "unordered",
            "items": [
              "It is a block-styled editor",
              "It returns clean data output in JSON",
              "Designed to be extendable and pluggable with a simple API"
            ]
          }
        },
        {
          "id": "OQmjP48IDR",
          "type": "header",
          "data": {
            "text": "What does it mean «block-styled editor»",
            "level": 3
          }
        },
        {
          "id": "q27q_u_XRR",
          "type": "paragraph",
          "data": {
            "text": "Workspace in classic editors is made of a single contenteditable element, used to create different HTML markups. Editor.js <mark class=\"cdx-marker\">workspace consists of separate Blocks: paragraphs, headings, images, lists, quotes, etc</mark>. Each of them is an independent contenteditable element (or more complex structure) provided by Plugin and united by Editor's Core."
          }
        },
        {
          "id": "VS3AEvB27H",
          "type": "paragraph",
          "data": {
            "text": "There are dozens of <a href=\"https://github.com/editor-js\">ready-to-use Blocks</a> and the <a href=\"https://editorjs.io/creating-a-block-tool\">simple API</a> for creation any Block you need. For example, you can implement Blocks for Tweets, Instagram posts, surveys and polls, CTA-buttons and even games."
          }
        },
        {
          "id": "tVUDbCg9Qs",
          "type": "header",
          "data": {
            "text": "What does it mean clean data output",
            "level": 3
          }
        },
        {
          "id": "Ba1WZUdHIU",
          "type": "paragraph",
          "data": {
            "text": "Classic WYSIWYG-editors produce raw HTML-markup with both content data and content appearance. On the contrary, Editor.js outputs JSON object with data of each Block. You can see an example below"
          }
        },
        {
          "id": "TsqWGDsRim",
          "type": "paragraph",
          "data": {
            "text": "Given data can be used as you want: render with HTML for <code class=\"inline-code\">Web clients</code>, render natively for <code class=\"inline-code\">mobile apps</code>, create markup for <code class=\"inline-code\">Facebook Instant Articles</code> or <code class=\"inline-code\">Google AMP</code>, generate an <code class=\"inline-code\">audio version</code> and so on."
          }
        },
        {
          "id": "i9zVExuMl-",
          "type": "paragraph",
          "data": {
            "text": "Clean data is useful to sanitize, validate and process on the backend."
          }
        },
        {
          "id": "Xv6RD8rkSV",
          "type": "delimiter",
          "data": {}
        },
        {
          "id": "Z6vnVOtzKm",
          "type": "paragraph",
          "data": {
            "text": "We have been working on this project more than three years. Several large media projects help us to test and debug the Editor, to make it's core more stable. At the same time we significantly improved the API. Now, it can be used to create any plugin for any task. Hope you enjoy. 😏"
          }
        },
        {
          "id": "RkpqsRMv63",
          "type": "image",
          "data": {
            "file": {
              "url": "https://codex.so/public/app/img/external/codex2x.png"
            },
            "caption": "",
            "withBorder": false,
            "stretched": false,
            "withBackground": false
          }
        }
      ]
    },
    created_at: faker.datatype.datetime(),
    created_by: {
      id: casual.integer(1, 1000),
      name: casual.full_name
    },
    like: faker.datatype.boolean(),
    likes_count: casual.integer(1, 100),
    reply_count: casual.integer(1, 100),
    favorite: faker.datatype.boolean()
  };

  posts.push(post);
}

export default posts;

export const post = posts[0]
