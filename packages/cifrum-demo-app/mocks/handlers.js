import { rest } from "msw";

import experts from "./data/experts";
import posts from "./data/posts";
import { post } from "./data/posts";
import comments from "./data/comments";

const API_BASE_URL = process.env.apiBaseUrl;

// ToDo: разобраться, как вызывать тут методы из store
// export default function ({ store, route, app: { $axios } }) {
//   console.log( $axios.defaults.headers.common.Authorization )
//     [...]
// }
export const handlers = [
  rest.get(`${API_BASE_URL}/experts`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        message: "Список экспертов",
        data: experts
      })
    );
  }),
  rest.get(`${API_BASE_URL}/users`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        message: "Список пользователей",
        data: experts
      })
    );
  }),
  rest.get(`${API_BASE_URL}/posts`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        message: "Список постов",
        data: posts
      })
    );
  }),
  rest.get(`${API_BASE_URL}/posts/:postId`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json(post)
    );
  }),
  rest.get(`${API_BASE_URL}/posts/:postId/comments`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        message: "Список комментариев",
        data: comments
      })
    );
  })
];
