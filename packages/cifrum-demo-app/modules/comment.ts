function parse(data?: Partial<Types.Comment>): Types.Comment {
  return {
    id: data!.id || 0,
    content: data!.content || {},
    created_at: data!.created_at || "",
    created_by: data!.created_by || null,
    parent: data!.parent || null,
    reply_count: data!.reply_count || 0,
    likes: data!.likes || 0,
    like: data!.like || false,
    post_id: data!.post_id || 1 // ToDo: временно
  }
}

// другие функции модуля

export default {
  // ... другие экспортированные функции
  parse,
}
