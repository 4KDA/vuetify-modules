function parse(data?: Partial<Types.Post>): Types.Post {
  return {
    id: data!.id || 0,
    title: data!.title || "",
    img: data!.img || "",
    tags: data!.tags || [],
    content: data!.content || null,
    created_at: data!.created_at || "",
    created_by: data!.created_by || null,
    like: data!.like || false,
    likes_count: data!.likes_count || 0,
    reply_count: data!.reply_count || 0,
    favorite: data!.favorite || false,
  }
}

// другие функции модуля

export default {
  // ... другие экспортированные функции
  parse,
}
