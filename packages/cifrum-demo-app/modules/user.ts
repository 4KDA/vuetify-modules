function parse(data?: Partial<Types.User>): Types.User {
  return {
    id: data!.id || 0,
    name: data!.name || "",
    avatar: data!.avatar || "",
    position: data!.position || "",
    favorite: data!.favorite || false,
  }
}

// другие функции модуля

export default {
  // ... другие экспортированные функции
  parse,
}
