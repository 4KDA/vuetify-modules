const path = require('path')

import ru from 'vuetify/lib/locale/ru'
import en from 'vuetify/lib/locale/en'
import CFR_ICONS from '@4kda/vuetify-cifrum-icons/lib/vuetify-cifrum-icons.js'
import config from './configs'

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // The server Property (https://nuxtjs.org/guides/configuration-glossary/configuration-server)
  server: {
    port: process.env.SERVER_PORT || 3000, // default: 3000
    host: process.env.SERVER_HOST || 'localhost', // default: localhost
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - intellectum',
    title: 'intellectum',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: ''},
      {name: 'format-detection', content: 'telephone=no'},
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
      {
        rel: 'preconnect',
        href: 'https://fonts.googleapis.com',
      },
      {
        rel: 'preconnect',
        href: 'https://fonts.gstatic.com',
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Inter:wght@100;200;400;500;600;700;900&display=swap',
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/scss/vuetify/custom',
    '~/assets/scss/transitions',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/vue-tippy',
    '~/plugins/vue-events',
    '~/plugins/vee-validate.js',
    '~/plugins/cifrum-icons.js',
    '~/plugins/cifrum-components.js',
    '~/plugins/vue-clipboard2.js',
    '~/plugins/globalFunctions.js',
    '~/plugins/msw.js',
    '~/services/index.js',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: [
    '~/components/auto', // shortcut to { path: '~/components/auto' }
    '~/components/form',
  ],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    // https://github.com/nuxt-community/moment-module
    '@nuxtjs/moment',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // Important first - 'vuetify-dialog/nuxt'
    // https://github.com/yariksav/vuetify-dialog
    'vuetify-dialog/nuxt',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://i18n.nuxtjs.org/
    '@nuxtjs/i18n',
    // https://portal-vue.linusb.org/
    'portal-vue/nuxt',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: process.env.apiBaseUrl,
    credentials: true,
    headers: {
      common: {
        Accept: 'application/json, */*',
      },
    },
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: config.locales.locale,
    },
  },

  // i18n module configuration: https://i18n.nuxtjs.org/
  i18n: {
    defaultLocale: config.locales.locale,
    locales: config.locales.availableLocales,
    vueI18n: {
      fallbackLocale: config.locales.fallbackLocale,
      messages: config.locales.messages,
    },
    // strategy: 'no_prefix'
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/scss/vuetify/variables'],
    customProperties: true,
    treeShake: true,
    theme: config.theme,
    icons: {
      values: {
        ...CFR_ICONS,
        dropdown: CFR_ICONS.chevronDown,
        expand: CFR_ICONS.chevronDown,
        clear: CFR_ICONS.close,
        prev: CFR_ICONS.chevronLeft,
        next: CFR_ICONS.chevronRight,
        checkboxOn: CFR_ICONS.checkboxOn,
        checkboxOff: CFR_ICONS.checkboxOff,
        checkboxIndeterminate: CFR_ICONS.checkboxIndeterminate,
      },
    },
    lang: {
      current: config.locales.locale,
      locales: {
        ru: {
          ...ru,
          dataIterator: {loadingText: 'Загрузка... Пожалуйста, подождите'},
        },
        en,
      },
      // To use Vuetify own translations without Vue-i18n comment the next line
      // t: (key, ...params) => this.i18n.t(key, params)
    },
  },

  // Moment module configuration: https://github.com/nuxt-community/moment-module
  moment: {
    defaultLocale: 'ru',
    locales: ['ru'],
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extend(config, {isClient}) {
      // Extend only webpack config for client-bundle
      if (isClient) {
        config.devtool = 'source-map'
      }
      config.resolve.alias['vue'] = path.resolve('./node_modules/vue')
    },
  },

  env: {
    apiBaseUrl: process.env.API_BASE_URL || 'http://localhost:3000',
    apiMocking: process.env.API_MOCKING || ''
  }
}
