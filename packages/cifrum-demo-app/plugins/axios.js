import Vue from 'vue'
// ToDo: не подключен, перебрать и подключить
export default function ({ app, $axios, redirect }, inject) {
  // Create a custom axios instance
  const api = $axios.create({
    headers: {
      common: {
        Accept: 'application/json, */*',
      },
    },
  })

  // Set baseURL to something different
  api.setBaseURL(process.env.NUXT_ENV_API_URL)

  // Inject to context as $api
  inject('api', api)

  if (process.client) {
    // Add context $api to $dialog
    app.context.$dialog.context.$api = api
    // or
    // Vue.prototype.$dialog.context.$api = api
  }

  const types = [$axios, api]
  types.forEach((element) => {
    element.onError((error) => {
      if (error.response) {
        // eslint-disable-next-line no-console
        console.log(error.response)
      }

      const code = parseInt(error.response && error.response.status)
      // if (code === 400) {
      //   redirect('/400')
      // }
      // if (code === 404) {
      //   redirect('/error/not-found')
      // } else if (code === 500) {
      //   redirect('/error/unexpected')
      // }
      if (
        !(
          error.response.config.url ===
            `${process.env.NUXT_ENV_BACK_URL}/api/user` && code === 401
        ) &&
        !(
          error.response.config.url ===
          `${process.env.NUXT_ENV_BACK_URL}/auth/login`
        )
      ) {
        const listErrorCodes = [422, 401, 403, 500]
        if (listErrorCodes.find((i) => i === code)) {
          let message = ''
          if (error.response.data.errors) {
            message = '<ul class="pl-3">'
            // eslint-disable-next-line array-callback-return
            Object.keys(error.response.data.errors).map(function (
              objectKey,
              index
            ) {
              message +=
                '<li>' + error.response.data.errors[objectKey] + '</li>'
            })
            message += '</ul>'
          } else {
            message = error.response.data.message
          }

          Vue.prototype.$dialog.notify.error(message, {
            position: 'bottom-right',
            timeout: 5000,
          })
        }
      }
    })

    element.onResponse((response) => {
      const { config } = response
      if (!config.headers.hideMessage) {
        const listMethods = ['post', 'patch', 'delete']
        if (
          listMethods.find((i) => i === config.method) &&
          response.data.message
        ) {
          Vue.prototype.$dialog.notify.success(response.data.message, {
            position: 'bottom-right',
            timeout: config.headers.notificationTimeout,
          })
        }
      }
    })
  })
}
