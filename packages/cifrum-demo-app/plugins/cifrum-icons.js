import Vue from 'vue'

const requireComponent = require.context(
  '@4kda/vuetify-cifrum-icons/lib/components/icons/',
  false,
  /\.(js|vue)$/i,
  'lazy'
)

requireComponent.keys().forEach((componentFilePath) => {
  const componentName = componentFilePath.split('/').pop().split('.')[0]
  Vue.component(`CfrIcon${componentName}`, () =>
    import(`@4kda/vuetify-cifrum-icons/lib/components/icons/${componentName}`)
  )
})
