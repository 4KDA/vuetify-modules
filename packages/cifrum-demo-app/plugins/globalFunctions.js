export default ({ app }, inject) => {
  // Inject $hello(msg) in Vue, context and store.
  inject('getInitials', (name) => {
    const rgx = /(\p{L}{1})\p{L}+/gu
    let initials = [...name.matchAll(rgx)] || []
    initials = ((initials.shift()?.[1] || '') + (initials.pop()?.[1] || '')).toUpperCase()
    return initials
  })
  inject('getColorByString', (string) => {
    const colors = [
      '#FFB900',
      '#D83B01',
      '#B50E0E',
      '#E81123',
      '#B4009E',
      '#5C2D91',
      '#0078D7',
      '#00B4FF',
      '#008272',
      '#107C10'
    ]
    let sum = 0
    let index
    for (index in string) {
      const r = string.toString()
      sum += r.charCodeAt(index)
    }
    return colors[sum % colors.length]
  })
  inject('getHumanDateUpdated', (date = new Date(), formatDate = 'D MMMM YYYY', withoutSuffix = false) => {
    const mdate = app.$moment(date || new Date())
    const diff = app.$moment().diff(mdate, 'days')

    if (diff > 7 || diff < 0) {
      return mdate.format(formatDate).replace(/\.$/, '') // '11 янв' etc.
      // } else if (diff === 7) {
      //   return 'Неделя'
    } else if (diff >= 2) {
      return mdate.fromNow(withoutSuffix) // '2 дня назад' etc.
    }
    // return mdate.calendar().split(', ')[0] // 'Сегодня' etc.
    return mdate.calendar(); // 'Сегодня, в 19:50' etc.
  })
  inject('isValidDate', (date) => {
    return date instanceof Date && !isNaN(date)
  })
  inject('declOfNum', (number, words) => {
    return words[(number % 100 > 4 && number % 100 < 20) ? 2 : [2, 0, 1, 1, 1, 2][(number % 10 < 5) ? Math.abs(number) % 10 : 5]]
  })
}
