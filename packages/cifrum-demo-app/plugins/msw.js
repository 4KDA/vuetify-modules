export default () => {
  if (process.env.apiMocking === "enabled") {
    const { worker } = require("../mocks/browser");
    console.log('process.env.apiBaseUrl', process.env.apiBaseUrl);
    worker.start({
      onUnhandledRequest(req) {
        // Выводим сообщение, если находим запрос к API к которому нет моков
        if (req.url.href === process.env.apiBaseUrl) {
          console.warn(`[MSW] Found an unhandled ${req.method} request to ${req.url.href}`);
        }
      }
    });
  }
};
