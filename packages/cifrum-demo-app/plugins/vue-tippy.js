import Vue from "vue"
import VueTippy from 'vue-tippy/dist/vue-tippy.esm';

Vue.use(VueTippy, {
  arrow: true,
  arrowType: 'round',
  placement: 'top', boundary: 'window'
})
