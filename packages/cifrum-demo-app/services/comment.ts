import Comment from "../modules/comment";

const API_ENDPOINT = '/comments'

// @ts-ignore
export default (app) => {
  return {
    async getComment(commentId: number): Promise<Types.Comment> {
      const { data } = await app.$axios.get(`${API_ENDPOINT}/${commentId}`);
      return Comment.parse(data);
    },

    async get(): Promise<Types.Comment[]> {
      const { data } = await app.$axios.$get(API_ENDPOINT)
      return Array.isArray(data) ? data.map(Comment.parse) : []
    }
  }
}
