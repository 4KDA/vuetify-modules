import UserService from "./user";
import PostService from "./post";

export default ({ app }, inject) => {
  const services = {
    users: UserService(app),
    posts: PostService(app),
  };

  inject("api", services);

  if (process.client) {
    // Add context $api to $dialog
    app.context.$dialog.context.$api = services
  }
}
