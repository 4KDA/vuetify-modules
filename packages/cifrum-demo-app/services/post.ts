import Post from "../modules/post";
import Comment from "../modules/comment";

const API_ENDPOINT = '/posts'

// @ts-ignore
export default (app) => {
  return {
    async createPost(invoice: Types.Post): Promise<Types.Post> {
      const { data } = await app.$axios.post(API_ENDPOINT, invoice);
      // парсинг данных и их возвращение в виде правильно сформированного объекта Post
      return Post.parse(data);
    },

    async getPost(postId: number): Promise<Types.Post> {
      const { data } = await app.$axios.get(`${API_ENDPOINT}/${postId}`);
      return Post.parse(data);
    },

    async updatePost(
      postId: number,
      updatedPost: Types.Post
    ): Promise<Types.Post> {
      const { data } = await app.$axios.put(`${API_ENDPOINT}/${postId}`, updatedPost);
      return Post.parse(data);
    },

    async deletePost(postId: number) {
      await app.$axios.delete(`${API_ENDPOINT}/${postId}`);
    },

    async get(): Promise<Types.Post[]> {
      const { data } = await app.$axios.$get(API_ENDPOINT)
      return Array.isArray(data) ? data.map(Post.parse) : []
    },

    async getPostComments(postId: number): Promise<Types.Comment[]> {
      const { data } = await app.$axios.$get(`${API_ENDPOINT}/${postId}/comments`)
      return Array.isArray(data) ? data.map(Comment.parse) : []
    },
  }
}
