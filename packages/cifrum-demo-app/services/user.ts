import User from "../modules/user";

const API_ENDPOINT = '/users'

// @ts-ignore
export default (app) => {
  return {
    async createUser(invoice: Types.User): Promise<Types.User> {
      const { data } = await app.$axios.post(API_ENDPOINT, invoice);
      // парсинг данных и их возвращение в виде правильно сформированного объекта User
      return User.parse(data);
    },

    async getUser(userId: number): Promise<Types.User> {
      const { data } = await app.$axios.get(`${API_ENDPOINT}/${userId}`);
      return User.parse(data);
    },

    async updateUser(
      userId: number,
      updatedUser: Types.User
    ): Promise<Types.User> {
      const { data } = await app.$axios.put(`${API_ENDPOINT}/${userId}`, updatedUser);
      return User.parse(data);
    },

    async deleteUser(userId: number) {
      await app.$axios.delete(`${API_ENDPOINT}/${userId}`);
    },

    async get(): Promise<Types.User[]> {
      const { data } = await app.$axios.$get(API_ENDPOINT)
      return Array.isArray(data) ? data.map(User.parse) : []
    }
  }
}
