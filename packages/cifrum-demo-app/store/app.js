export const state = () => ({
  rightDrawer: false,
  alphabet: ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я']
})

export const mutations = {
  toggleRightDrawer(state, payload) {
    if (typeof payload !== 'undefined') {
      state.rightDrawer = payload
    } else {
      state.rightDrawer = !state.rightDrawer
    }
  }
}

export const actions = {}

export const getters = {}
