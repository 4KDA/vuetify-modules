export const state = () => ({
  comments: [],
  list: []
});

export const mutations = {
  setList (state, payload) {
    state.list = payload
  },
  setComments (state, payload) {
    state.comments = payload
  },
  addComment (state, payload) {
    state.comments.unshift(payload)
  },
  toggleLikeComment (state, commentId) {
    const comment = state.comments.find(i => i.id === commentId)
    if(comment) {
      comment.like = !comment.like
    }
  },
};

export const actions = {};

export const getters = {
  getCommentsByParentId: state => id => {
    return state.comments.filter(i => i.parent === id)
  },
  getCountCommentsByPostId: state => id => {
    return state.comments.length
    // return state.comments.filter(i => i.post_id === id).length
  },
};
