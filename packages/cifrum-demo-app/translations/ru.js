// import ru from 'vuetify/lib/locale/ru'

export default {
  common: {
    add: 'Добавить',
    cancel: 'Отмена',
    description: 'Описание',
    delete: 'Удалить',
    title: 'Заголовок',
    save: 'Сохранить',
    faq: 'FAQ',
    contact: 'Связаться с нами',
    tos: 'Условия обслуживания',
    policy: 'Политика конфиденциальности'
  },
  usermenu: {
    profile: 'Профиль',
    signin: 'Войти в систему',
    dashboard: 'Аналитическая панель',
    signout: 'Выйти'
  },
  error: {
    notfound: 'Страница не найдена',
    other: 'Произошла ошибка'
  },
  check: {
    title: 'Смена пароля',
    backtosign: 'Вернуться к входу в систему',
    newpassword: 'Новый пароль',
    button: 'Сменить пароль и войти в систему',
    error: 'Ссылка на действие недействительна',
    verifylink: 'Проверка ссылки...',
    verifyemail: 'Проверка адреса электронной почты...',
    emailverified: 'Электронная почта подтверждена! Перенаправление...'
  },
  forgot: {
    title: 'Забыли пароль?',
    subtitle: 'Введите адрес электронной почты своей учетной записи, и мы отправим вам ссылку для сброса пароля.',
    email: 'Email',
    button: 'Запросить сброс пароля',
    backtosign: 'Вернуться к входу'
  },
  login: {
    title: 'Войти в систему',
    email: 'Email',
    login: 'Логин',
    password: 'Пароль',
    button: 'Войти',
    orsign: 'Или войдите с помощью',
    forgot: 'Забыли пароль?',
    noaccount: 'Нет учетной записи?',
    create: 'Создайте здесь',
    error: 'Комбинация адреса электронной почты и пароля недействительна',
    welcome: 'Добро пожаловать',
    signInToYourAccount: 'Войдите в свой аккаунт'
  },
  register: {
    title: 'Регистрация',
    name: 'Полное имя',
    email: 'Email',
    password: 'Пароль',
    button: 'Регистрация',
    orsign: 'Или зарегистрируйтесь с',
    agree: 'Регистрируясь, вы соглашаетесь с',
    account: 'Уже есть аккаунт?',
    signin: 'Войти в систему'
  },
  utility: {
    maintenance: 'Обслуживаемся'
  },
  menu: {
    search: 'Поиск (нажмите \'ctrl + /\' для ввода)',
    dashboard: 'Аналитическая панель',
    logout: 'Выйти',
    profile: 'Профиль',
    blank: 'Пустая страница',
    pages: 'Страницы',
    entities: 'Сущности',
    others: 'Другие',
    email: 'Email',
    chat: 'Чат',
    todo: 'Сделать',
    board: 'Доска задач',
    users: 'Пользователи',
    usersList: 'Список',
    usersEdit: 'Редактировать',
    auth: 'Аутентификация',
    authLogin: 'Вход',
    authRegister: 'Регистрация',
    authVerify: 'Подтвердить Email',
    authForgot: 'Забыли пароль',
    authReset: 'Сброс пароля',
    errorPages: 'Ошибки',
    errorNotFound: 'Не найдено / 404',
    errorUnexpected: 'Неожиданно / 500',
    utilityPages: 'Вспомогательные',
    utilityMaintenance: 'Обслуживание',
    utilitySoon: 'Скоро заработаем',
    utilityHelp: 'FAQ / Помощь',
    levels: 'Уровни меню',
    disabled: 'Меню отключено',
    docs: 'Документация',
    feedback: 'Обратная связь',
    support: 'Поддержка',
    administration: 'Администрирование',
    roles: 'Роли',
    security: 'Безопасность',
    journals: 'Журналы',
    objects: 'Объекты'
  }
  // Vuetify components translations
  // $vuetify: ru
}
