interface createdBy {
  id: number,
  name: string,
  avatar: string,
  position: string
}

namespace Types {
  export interface Comment {
    id: number,
    content: object
    created_at: string,
    created_by: createdBy | null,
    reply_count: number
    likes: number,
    like: boolean,
    parent: number | null,
    post_id: number
  }
}
