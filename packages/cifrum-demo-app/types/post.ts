interface createdBy {
  id: number,
  name: string
}

namespace Types {
  export interface Post {
    id: number
    title: string
    img: string
    tags: string[]
    content: object | null
    created_at: string
    created_by: createdBy | null
    like: boolean
    likes_count: number
    reply_count: number
    favorite: boolean
  }
}
