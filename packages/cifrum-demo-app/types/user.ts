namespace Types {
  export interface User {
    id: number
    name: string
    avatar: string
    position: string
    favorite: boolean
  }
}
