# `@4kda/vuetify-cifrum-icons`

The Cifrum icons for Vuetify.

## Install

```
$ npm install @4kda/vuetify-cifrum-icons
```
or
```
$ yarn add @4kda/vuetify-cifrum-icons
```

## Usage

```js
import Vue from 'vue'

const requireComponent = require.context(
  '@4kda/vuetify-cifrum-icons/lib/components/icons/',
  false,
  /\.(js|vue)$/i,
  'lazy'
)

requireComponent.keys().forEach((componentFilePath) => {
  const componentName = componentFilePath.split('/').pop().split('.')[0]
  Vue.component(`CfrIcon${componentName}`, () =>
    import(`@4kda/vuetify-cifrum-icons/lib/components/icons/${componentName}`)
  )
})

```


```js
import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import CFR_ICONS from '@4kda/vuetify-cifrum-icons/lib/vuetify-cifrum-icons.js'

Vue.use(Vuetify);

const requireComponent = require.context(
  '@4kda/vuetify-cifrum-icons/lib/components/icons/',
  false,
  /\.(js|vue)$/i,
  'lazy'
)

requireComponent.keys().forEach((componentFilePath) => {
  const componentName = componentFilePath.split('/').pop().split('.')[0]
  Vue.component(`CfrIcon${componentName}`, () =>
    import(`@4kda/vuetify-cifrum-icons/lib/components/icons/${componentName}`)
  )
})

export default new Vuetify({
  icons: {
    values: {
      ...CFR_ICONS,
      dropdown: CFR_ICONS.chevronDown,
      clear: CFR_ICONS.close,
    },
  },
});
```

## Usage Nuxt

In nuxt.config.js

```js
import CFR_ICONS from '@4kda/vuetify-cifrum-icons/lib/vuetify-cifrum-icons.js'

export default {
  plugins: [
    '@4kda/vuetify-cifrum-icons/lib/components.js',
  ],

  vuetify: {
    icons: {
      values: {
        ...CFR_ICONS,
        dropdown: CFR_ICONS.chevronDown,
        clear: CFR_ICONS.close,
        prev: CFR_ICONS.chevronLeft,
        next: CFR_ICONS.chevronRight,
        checkboxOn: CFR_ICONS.checkboxOn,
        checkboxOff: CFR_ICONS.checkboxOff,
        checkboxIndeterminate: CFR_ICONS.checkboxIndeterminate,
      }
    }
  }
}
```
