import Vue from 'vue'

const requireComponent = require.context(
  './components/icons/',
  false,
  /\.(js|vue)$/i,
  'lazy'
)

requireComponent.keys().forEach((componentFilePath) => {
  const componentName = componentFilePath.split('/').pop().split('.')[0]
  Vue.component(`CfrIcon${componentName}`, () =>
    import(`./components/icons/${componentName}`)
  )
})
