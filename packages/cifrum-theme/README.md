# `@4kda/vuetify-cifrum-theme`

The Cifrum theme for Vuetify.

## Install

```
$ npm install @4kda/vuetify-cifrum-theme
```

## Usage

```js
import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import theme from '@4kda/vuetify-cifrum-theme'

Vue.use(Vuetify);

export default new Vuetify({
  theme
});
```
