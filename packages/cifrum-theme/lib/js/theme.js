export default {
  options: {
    customProperties: true
  },
  themes: {
    dark: {
      background: '#05090c',
      surface: '#282828',
      primary: '#4C9AFF',
      secondary: '#C9E1FF',
      accent: '#82B1FF',
      error: '#FF5252',
      info: '#2196F3',
      success: '#82CA8A',
      warning: '#FFC107',
    },
    light: {
      background: '#ffffff',
      surface: '#F3F5F5',
      primary: '#4C9AFF',
      secondary: '#C9E1FF',
      accent: '#048ba8',
      error: '#ef476f',
      info: '#2196F3',
      success: '#82CA8A',
      warning: '#ffd166',
    }
  }
}
